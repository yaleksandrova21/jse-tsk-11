package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findById(final String id) {
        for(Project project:projects){
            if(id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(String name) {
        for(Project project:projects){
            if(name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        projects.remove(project);
        return project;
    }

}
