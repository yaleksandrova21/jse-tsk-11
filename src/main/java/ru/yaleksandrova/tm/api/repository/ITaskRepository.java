package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.model.Task;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

}
