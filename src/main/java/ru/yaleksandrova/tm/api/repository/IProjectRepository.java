package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

}
