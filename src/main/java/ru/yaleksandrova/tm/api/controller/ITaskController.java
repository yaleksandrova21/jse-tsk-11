package ru.yaleksandrova.tm.api.controller;

import ru.yaleksandrova.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void clearTask();

    void showTask(Task task);

    void createTask();

    void showById();

    void showByIndex();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

}
