package ru.yaleksandrova.tm.api.controller;

import ru.yaleksandrova.tm.model.Project;

public interface IProjectController {

    void showProjects();

    void clearProject();

    void createProject();

    void showById();

    void showByIndex();

    void showProject(Project project);

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

}
