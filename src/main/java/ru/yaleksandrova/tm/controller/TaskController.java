package ru.yaleksandrova.tm.controller;

import ru.yaleksandrova.tm.api.controller.ITaskController;
import ru.yaleksandrova.tm.api.sevice.ITaskService;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task: tasks) System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTask(Task task) {
        if (task == null)
            return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        this.taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER ID]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.findByIndex(index);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        showTask(task);
    }

    @Override
    public void removeById() {
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.removeByIndex(index);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Task task  = taskService.removeByName(name);
        if (task == null){
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Task deleted]");
        showTask(task);
    }

    @Override
    public void updateById() {
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Task task  = taskService.findById(id);
        if (task == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Task taskUpdatedId = taskService.updateById(id, name, description);
        if (taskUpdatedId == null) {
            return;
        }
        System.out.println("[Task updated]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Task taskUpdatedIndex = taskService.updateByIndex(index, name, description);
        if (taskUpdatedIndex == null) {
            return;
        }
        System.out.println("[Task updated]");
    }

}
