package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.api.sevice.ITaskService;
import ru.yaleksandrova.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }


    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if(id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(String name) {
        if(name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(Integer index) {
        if(index == null || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty() || name == null || name.isEmpty())
            return null;
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || name == null || name.isEmpty())
            return null;
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0)
            return null;
        return taskRepository.removeByIndex(index);
    }

}
